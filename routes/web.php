<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');
Route::prefix('configurateur')->group(function () {
    Route::get('/', 'ConfiguratorController@index')->name('configurator');
    Route::get('/pure', 'ConfiguratorController@pure')->name('pure');
    Route::get('/legende', 'ConfiguratorController@legende')->name('legende');
    });
