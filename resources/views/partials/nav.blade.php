<style>
    .nav-alpine {
        background-color: rgb(1, 190, 254);
    }
    .nav-link {
        color: white !important;
        font-family: "alpine-air";
    }
    .nav-link:hover {
        background-color: white;
        color: rgb(1, 190, 254) !important;
    }
</style>


    <nav class="navbar navbar-expand-lg navbar-light fixed-top nav-alpine">
            <img class="img-fluid alpine d-block mx-auto" src="{{ asset('assets/sources-homepage/logo/logo-white.png') }}"
            id="logo">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse pl-1" id="navbarNav">
            <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="{{route('home')}}">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#agility">Agilité</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#design">Design</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#conception">Conception</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#motorisation">Motorisation</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#technology">Technologie</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#inside">Intérieur</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#features">Caractéristiques techniques</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#versions">Versions</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#gallery">Gallerie Photo</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('configurator') }}">Configurateur</a>
            </li>
            </ul>
        </div>
    </nav>
{{-- </div> --}}
