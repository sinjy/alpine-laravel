<style>

/* Navbar */
/* Add a black background color to the top navigation */
.topnav {
    position: relative;
    background-color: rgba(1, 190, 254, 0.4);
    overflow: hidden;
    z-index: 9;
}

/* Style the links inside the navigation bar */
.topnav a {
    float: left;
    display: block;
    color: #f2f2f2;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    font-size: 14px;
}

/* Change the color of links on hover */
.topnav a:hover {
    background-color: #ddd;
    color: black;
}

/* Add a color to the active/current link */
.topnav a.active {
  /* background-color: #4CAF50; */
    color: white;
}

/* Centered section inside the top navigation */
.topnav-centered a {
    float: none;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}

/* Right-aligned section inside the top navigation */
.topnav-right {
    float: right;
}
.sticky {
    position: fixed;
    top: 0;
    width: 100%;
}
.dropdown {
    float: left;
    overflow: hidden;
}

.dropdown .dropbt {
    font-size: 17;
    border: none;
    outline: none;
    color: white;
    padding: 14px 16px;
    background-color: inherit;
    font-family: inherit;
    margin: 0;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 9;
}

.dropdown-content a {
    float: none;
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align: left;
}

/* Fin navbar */

/* Responsive navigation menu - display links on top of each other instead of next to each other (for mobile devices) */
@media screen and (max-width: 1050px) {
    .desktop-nav a, .topnav-right {
        float: none;
        display: none;
    }
    #logo {
        max-width: 70%;
    }
    .topnav-right,a {
        font-size: 12px !important;
    }

    .topnav-centered a {
        position: relative;
        top: 0;
        left: 0;
        transform: none;
    }
}

</style>




<header>
    <!-- Top navigation -->
    <div class="topnav" id="myTopnav">
        <div class="row">
            <div class="col-3">
                <!-- Navbar Hamburger -->
                <div class="pos-f-t d-md-block d-lg-none">
                    <div class="collapse" id="navbarToggleExternalContent">
                        <div class="p-4">
                            <ul>
                                <a class="cursor" href="#"><li>Accueil</li></a>
                                <a class="cursor" href="#agility"><li>Agilité</li></a>
                                <a class="cursor" href="#design"><li>Design</li></a>
                                <a class="cursor" href="#conception"><li>Conception</li></a>
                                <a class="cursor" href="#motorisation"><li>Motorisation</li></a>
                                <a class="cursor" href="#technology"><li>Technologie</li></a>
                                <a class="cursor" href="#inside"><li>Intérieur</li></a>
                                <a class="cursor" href="#features"><li>Caractéristiques techniques</li></a>
                                <a class="cursor" href="#versions"><li>Versions</li></a>
                                <a class="cursor" href="#gallery"><li>Gallerie Photo</li></a>

                            </ul>
                        </div>
                    </div>
                    <nav class="navbar navbar-dark">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent"
                                aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </nav>
                </div>
            </div>
            <!--end of hamburger-->
            <div class="col-6 col-lg-12">
                <!-- Centered link -->
                <div class="topnav-centered mt-4">
                    <img class="img-fluid alpine d-block mx-auto" src="{{ asset('assets/sources-homepage/logo/logo-white.png') }}"
                    id="logo">
                </div>
            </div>
        </div>
        <div class="row desktop-nav mt-1">
            <div class="col-12 ml-5">
                <!-- Right-aligned links -->
                <a href="#">Accueil</a>
                <a href="#agility">Agilité</a>
                <a href="#design">Design</a>
                <a href="#conception">Conception</a>
                <a href="#motorisation">Motorisation</a>
                <a href="#technology">Technologie</a>
                <a href="#inside">Intérieur</a>
                <a href="#features">Caractéristiques Techniques</a>
                <a href="#versions">Versions</a>
                <a href="#gallery">Gallerie Photo</a>
            </div>
        </div>
    </div>
</header>

<script>
    // When the user scrolls the page, execute myFunction
window.onscroll = function() {myFunction()};

// Get the navbar
var navbar = document.getElementById("myTopnav");

// Get the offset position of the navbar
var sticky = navbar.offsetTop;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
    } else if (window.pageYOffset < sticky) {
        navbar.classList.remove("sticky");
    }
}

</script>