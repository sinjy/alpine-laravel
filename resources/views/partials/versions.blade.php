 <!--Section versions-->
 <section id="versions" class="bg-blue">
        <div class="container mt-5 pb-5">
            <div class="row">
                <div class="col-12 mt-5">
                    <h1 class="text-center text-white writing">Versions</h1>
                </div>
                <div class="row bg-light text-blue mt-5">
                    <div class="col-sm-6 mt-2 mb-2">
                        <h1 class="text-center">A110 Pure</h1>
                        <hr>
                        <p>Légère et conçue avant tout pour le plaisir du pilote, l’A110 Pure est la version la plus
                            fidèle à l’esprit de la mythique berlinette qui remporta le Rallye Monte-Carlo en 1973.
                            Équipée de sièges baquet Sabelt, l’A110 Pure combine agilité et précision de pilotage. À
                            l’allure volontairement sportive, cette version est habillée de sièges en cuir-microfibre,
                            les finitions intérieures pariant sur la fibre de carbone mat.</p>
                        <img src="{{ asset('assets/sources-homepage/versions/ALPINE-PURE-1.png') }}" class="img-fluid ml-5 mt-4" width="100%">
                    </div>
                    <div class="col-sm-6 mt-2 mb-2">
                        <h1 class="text-center">A110 Légende</h1>
                        <hr>
                        <p>Dotée du même groupe motopropulseur et des mêmes réglages de suspension que l'A110 Pure, la
                            version Légende est fidèle à la philosophie Alpine : l’agilité absolue. Affichant le
                            caractère d’une GT, l’A110 Légende se distingue par un choix d’assises grand confort avec
                            des sièges réglables à six voies et une sellerie en cuir noir ou brun. L’habitacle en fibre
                            de carbone brillant et les jantes 18 pouces Légende subliment la personnalité raffinée de
                            la version Légende. Quant aux capteurs de stationnement avec caméra de recul, ils procurent
                            à l’A110 Légende maniabilité et simplicité d’utilisation au quotidien.</p>
                        <img src="{{ asset('assets/sources-homepage/versions/ALPINE-LEGENDE-1.png') }}" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end of section-->