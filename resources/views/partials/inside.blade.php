<!--Section Intérieur-->
<section id="inside" class="bg-blue">
    <div class="container-fluid text-white ">
        <div class="row">
            <div class="col-12 ">
                <h1 class="text-center pt-5 writing">Intérieur</h1>
                <p class="lead px-5">A l’intérieur, l’émotion vient du contraste entre les matières chaudes et
                    les matières froides. Les éléments en aluminium apparents se marient avec du cuir. Le carbone
                    complète l’ambiance sportive. La présence d’une console centrale surélevée et de sièges baquets
                    légers vont de paire avec l’esprit de légèreté de la structure. La climatisation et la connexion au
                    smartphone garantissent eux liberté et confort.</p>
                <hr>
            </div>
            <div class="offset-sm-3 col-sm-6 pt-2 text-center">
                <h3>Les sièges</h3>
                <p>Avec seulement 13,1 kg chacun, les sièges baquet offrent un confort maximal en piste et participent
                    de l’extrême légèreté de l’A110. Des sièges six-voies réglables, toujours très légers, sont
                    également proposés en option.</p>
            </div>
            <!-- Slider -->
            <div>
            <div class="offset-sm-11 col-sm-1 pl-4 mt-3 ">
                <div id="demo" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <!-- <ul class="carousel-indicators">
                        <li data-target="#demo" data-slide-to="0" class="active"></li>
                        <li data-target="#demo" data-slide-to="1"></li>
                    </ul> -->
                    <!-- The slideshow -->
                    <div class="carousel-inner slide-height">
                        <div class="carousel-item active">
                            <img src="{{ asset('assets/sources-homepage/interieur/interieur_2_desktop.png') }}" class="img-fluid" alt="Slide-01">
                        </div>
                        <div class="carousel-item">
                            <img src="{{ asset('assets/sources-homepage/interieur/interieur_desktop.png') }}"class="img-fluid" alt="Slide-02">
                        </div>
                        <!-- Left and right controls -->
                        <!-- <a class="carousel-control-prev" href="#demo" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a class="carousel-control-next" href="#demo" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a> -->
                    </div>
                </div>
            <!-- Fin slider -->
        </div>
    </div>
</section>
<!--end of section-->